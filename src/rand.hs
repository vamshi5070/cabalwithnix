import System.Random
import System.IO

main :: IO ()
main = do
  secret_number <- getStdRandom (randomR (1,10)) :: IO Int
  -- putStr "Secret number: "
  hFlush stdout
  -- print secret_number
  helperFunction secret_number

helperFunction :: Int -> IO ()
helperFunction secret_number = do
  putStr "Guess a number: "
  guess <- (read <$> getLine) :: IO Int
  if guess > secret_number
    then do
           putStrLn "Large"
           helperFunction secret_number                  
    else if guess < secret_number
           then do
                 putStrLn "Small"
                 helperFunction secret_number
           else putStrLn "Equal"

  
