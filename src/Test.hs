module Test where
{-# LANGUAGE OverloadedStrings #-}
-- import Relude
-- import qualified Relude.Unsafe as Unsafe

inserts :: forall {a}. Ord a => a -> [a] -> [a]
inserts ele [] = [ele]
inserts ele (x:xs)
  | ele <= x =  ele:x:xs
  | otherwise = x: inserts ele xs

main :: IO ()
main = do
  print $ inserts 3 [1,2,4,5 :: Integer]  
