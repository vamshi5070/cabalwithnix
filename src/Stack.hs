data Stack a = Empty
               | Node a (Stack a)
               deriving (Eq,Show)

stack :: forall {a}. Num a =>  Stack a
stack = Node 3 (Node 4 Empty)
