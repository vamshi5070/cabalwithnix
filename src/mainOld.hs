module Main where

import Lens.Micro
import qualified Brick.Widgets.Edit as E
-- import Main.Utf8 qualified as Utf8
-- import Test
import Tui

 -- Main entry point.
 -- The `bin/run` script will invoke this function. See `.ghcid` file to change
 -- that.

main :: IO ()
main = tui
